
var ImgSource = [
	  "src/images/image1.jpg",
	  "src/images/image2.jpg",
	  "src/images/image3.jpg",
	  "src/images/image4.jpg",
	  "src/images/image5.jpg",
	  "src/images/image6.jpg",
	  "src/images/image7.jpg",
	  "src/images/image8.jpg",
	  "src/images/image9.jpg",
	  "src/images/image10.jpg"
];

/**
* Fungsi random number dari stackoverflow
*/

function acakGambar(){
	
	var arr = new Array();
	
	while(arr.length < 16){
		
		var randomnumber=Math.ceil(Math.random()*16)
		var found=false;
		
		for(var i=0; i < arr.length; i++){
			
			if(arr[i] == randomnumber){
				
				found=true;
				break;
			
			}
		 
		 }
		if(!found){
			
			arr[arr.length]=randomnumber;
		
		}
		
	}
	
	/**
	* random gambar berdasarkan random number yang diberikan
	*/
	
	for (i = 0; i < arr.length; i++){
		
		$("#" + (i + 1)).attr("src",ImgSource[(arr[i] % 8)+1]);
	
	}
		
}
	
var img1;
var isClick = false;
var benar = 0;
var klir = false;

/**
* Fungsi buka gambar 
*/

function bukaGambar(angka){
	
	if (!isClick){
		return;
	}
	
	if (img1 === undefined){
		img1 = $("#" + angka);
		lihatGambar(img1);
	}else {
		
		var img2 = $("#" + angka);
		lihatGambar(img2);
		
		if (img1.attr("id") === img2.attr("id")){
			return;
		}
		
		if (img1.attr("src") === img2.attr("src")){
			img1.attr("onclick","");
			img2.attr("onclick","");
			img1 = undefined;
			benar++;
			
			if (benar == 8){
				clearInterval(timer);
				alert("Selamat! Anda telah menyelesaikan Game Ini");
				rank();
				benar = 0;
				klir = true;
			}
			
		}else {
			isClick = false;
			setTimeout(function(){
				tutupGambar(img1);
				tutupGambar(img2);
				img1 = undefined;
				isClick = true;
			}, 500);
				
		}
		
	}
	
}
	
/**
* Fungsi lihat dan tutup gambar
*/	
	
function lihatGambar(img){
	img.css("opacity",100);
}

function tutupGambar(img){
	img.css("opacity",0);
}

var milsec = 0;
var second = 0;
var minute = 0;
var timer;
var time;

/**
* Fungsi timer atau stopwatch 
*/

function counter(){

	milsec++;
	
	if (milsec === 100){
		second++;
		milsec = 0;
	}
	if (second === 60){
		minute++;
		second = 0;
	}
	
	if (milsec >= 10){
		if (second >= 10){
			if (minute >= 10){
				time = minute+":"+second+":"+milsec;
			}else {
				time = "0"+minute+":"+second+":"+milsec;
			}
		}else {
			if (minute >= 10){
				time = minute+":0"+second+":"+milsec;
			}else {
				time = "0"+minute+":0"+second+":"+milsec;
			}
		}
	}else {
		if (second >= 10){
			if (minute >= 10){
				time = minute+":"+second+":0"+milsec;
			}else {
				time = "0"+minute+":"+second+":0"+milsec;
			}
		}else {
			if (minute >= 10){
				time = minute+":0"+second+":0"+milsec;
			}else {
				time = "0"+minute+":0"+second+":0"+milsec;
			}
		}
	}
	
	$("#time").html(time);

}

var started = false;

/**
* Fungsi untuk memulai timer
*/

function start() {
	if(!started){
		started = true;
		timer = setInterval(counter,10);
	}
}

var top5;

/**
* Fungsi untuk mengurutkan waktu
*/

function rank(){
	
	if (top5 == null){
		top5 = new Array();
	}
	
	top5.push(time);
	top5.sort();
	localStorage.setItem(time,sessionStorage.getItem("visitor"));
	localStorage.setItem("top5",JSON.stringify(top5));
	top5 = JSON.parse(localStorage.getItem("top5"));
	print();
	
}

/**
* Fungsi untuk mencetak leaderboard
*/

function print(){
	
	var i = 1;
	
	$.each(top5,function(key,value) {
					
		$("#user"+i).html(localStorage.getItem(this));
		$("#score"+i).html(this);
		i++;
		
	});
	
}

/**
* Fungsi untuk menghapus leaderboard
*/

function hapus(){
	
	if (klir){
		localStorage.setItem("top5","");
		localStorage.clear();
	
		for (var i = 1; i <= 5; i++){
			
			$("#user"+i).html("");
			$("#score"+i).html("");
			
		}
	}else {
		alert("Mainkan game terlebih dahulu");
	}
	
}

$(document).ready(function(){
	
	$("#play").click(function(){
		$("#play").hide();
		$("#loginContent").fadeIn("slow");
	});
	
	alert("Selamat Datang di game Tebak Gambar!");
	
	/**
	* Fungsi untuk meload json sesuai dengan yang diketikkan
	*/
	
	$("#loginButton").click(function(){
		
		$.getJSON("src/json/users.json",function(obj) {
			
			var namaUser = document.getElementById("username").value;
			var kataKunci = document.getElementById("password").value;
			
			var flag = true;
			
			var arr = obj.users; 
			for (var i = 0; i < arr.length; i++){
					
				if(arr[i].username == namaUser && arr[i].password == kataKunci){
					sessionStorage.setItem("visitor", namaUser);
					alert("LOGIN SUKSES");
					window.location = "main.html";
					i = arr.length;
					flag = false;
				}
				
			}
			
			if (flag){
				
				alert("LOGIN GAGAL");
				
			}
			
		});
		
	});
	
	$(".button").click(function(){
        
		if (document.getElementById("name").innerHTML === ""){
			alert("Harap Login Terlebih Dahulu");
			window.location = "login.html";
		}else {
			start();
			isClick = true;
		}
	
    });

	$(function() {
				
		acakGambar();
	
		document.getElementById("name").innerHTML = sessionStorage.getItem("visitor");
	
		top5 = JSON.parse(localStorage.getItem("top5"));
		print();
	
	 });
	
});